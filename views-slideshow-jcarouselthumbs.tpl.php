<?php

/**
 * @file
 * Views Slideshow: Thumbnail Hover template file.
 */
?>

<?php if (!empty($controls_top) || !empty($image_count_top) || !empty($breakout_top)): ?>
  <div class="views-slideshow-controls-top clear-block">
    <?php if (isset($controls_top)) { print $controls_top; } ?>
    <?php if (isset($pager_top)) { print $pager_top; } ?>
    <?php if (isset($breakout_top)) { print $breakout_top; } ?>
  </div>
<?php endif; ?>

<?php print $slideshow; ?>

<?php if (!empty($breakout_bottom) || !empty($controls_bottom) || !empty($image_count_bottom)): ?>
  <div class="views-slideshow-controls-bottom clear-block">
    <?php if (isset($breakout_bottom)) { print $breakout_bottom; } ?>
    <?php if (isset($controls_bottom)) { print $controls_bottom; } ?>
    <?php if (isset($image_count_bottom)) { print $image_count_bottom; } ?>
  </div>
<?php endif; ?>
